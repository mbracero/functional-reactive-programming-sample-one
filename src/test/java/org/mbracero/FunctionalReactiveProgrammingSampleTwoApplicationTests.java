package org.mbracero;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * https://spring.io/blog/2016/07/20/notes-on-reactive-programming-part-iii-a-simple-http-server-application
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FunctionalReactiveProgrammingSampleTwoApplicationTests {
	
	private static Logger LOGGER = LoggerFactory.getLogger(FunctionalReactiveProgrammingSampleTwoApplicationTests.class);
	
	@Before
	public void startUp() throws Exception {
		//
	}
	
	public static class Result {
		int result = 0;
		public void add (int value) {
			LOGGER.info("Adding : {}", value);
			result += value;
		}
		
		public void stop() {
			LOGGER.info("The result is : {}", result);
		}
	}
	
	private int block(int value) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    return 200;
	}

	@Test
	public void firstBlockingTest() {
		Flux.range(1, 10) // make 10 calls
		    .log()
		    .map(this::block) // blocking code here
		    .collect(Result::new, Result::add) // collect results and aggregate into a single object
		    .doOnSuccess(Result::stop) // at the end stop the clock (the result is a Mono<Result>)
		    .subscribe();
		
		/**
		 Don’t do this at home. It’s a "bad" example because, while the APIs are technically being used correctly,
		 	we know that it is going to block the calling thread; this code is more or less equivalent to a for
		 	loop with the call to block() in the body of the loop.
		 */
	}
	
	// The scheduler is declared separately as a shared field
	Scheduler scheduler = Schedulers.parallel();
	
	/**
	 A better implementation would push the call to block() onto a background thread. We can do that by wrapping
	 	it in a method that returns a Mono<Integer>
	 */
	private Mono<Integer> fetch(int value) {
	    return Mono.fromCallable(() -> block(value)) // blocking code here inside a Callable to defer execution
	        .subscribeOn(this.scheduler); // subscribe to the slow publisher on a background thread
	}
	
	// Then we can declare that we want to flatMap() the sequence instead of using map()
	@Test
	public void testUnBlockingTest() {		
		Flux.range(1, 10)
		    .log()
		    .flatMap( // drop down to a new publisher to process in parallel
		        this::fetch, 4) // concurrency hint in flatMap
		    .collect(Result::new, Result::add)
		    .doOnSuccess(Result::stop)
		    .subscribe();
	}
	
}
