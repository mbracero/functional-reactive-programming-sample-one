package org.mbracero;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * https://spring.io/blog/2016/06/13/notes-on-reactive-programming-part-ii-writing-some-code
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FunctionalReactiveProgrammingSampleOneApplicationTests {
	
	private static Logger LOGGER = LoggerFactory.getLogger(FunctionalReactiveProgrammingSampleOneApplicationTests.class);
	
	private static List<String> NAMES = Arrays.asList("juan", "pepe", "carmen", "rocio", "sara");
	
	private Flux<String> flux;
	
	@Before
	public void startUp() throws Exception {
		this.flux = Flux.fromIterable(NAMES);
	}

	@Test
	public void testOperator() {
		// to ask for the internal events inside a Flux to be logged to standard out
		flux
		  .log()
		  .map(String::toUpperCase);
		
		/**
		 Calling operators on a Flux amounts to building a plan of execution for later. It is completely
		 	declarative, and it’s why people call it "functional". The logic implemented in the operators is only
		 	executed when data starts to flow, and that doesn’t happen until someone subscribes to the Flux (or
		 	equivalently to the Publisher).
		 */
	}
	
	@Test
	public void testSuscribe() {		
		/**
		 To make the data flow you have to subscribe to the Flux using one of the subscribe() methods.
		 	Only those methods make the data flow. They reach back through the chain of operators you
		 	declared on your sequence (if any) and request the publisher to start creating data. In the
		 	sample samples we have been working with, this means the underlying collection of strings is iterated.
		 	In more complicated use case it might trigger a file to be read from the filesystem, or a pull from
		 	a database or a call to an HTTP service.
		 */
		flux
		  .log()
		  .map(String::toUpperCase)
		.subscribe();
	}
	
	@Test
	public void testConsume() {
		/**
		 The subscribe() method is overloaded, and the other variants give you different options to control what
		 	happens. One important and convenient form is subscribe() with callbacks as arguments. The first argument
		 	is a Consumer, which gives you a callback with each of the items, and you can also optionally add a
		 	Consumer for an error if there is one, and a vanilla Runnable to execute when the sequence is complete.
		 	For example, just with the per-item callback
		 */
		flux
		  .log()
		  .map(String::toUpperCase)
		.subscribe(LOGGER::info);
	}
	
	@Test
	public void testSubscription() {
		// to consume at most 2 items at a time
		flux.log().map(String::toUpperCase).subscribe(new Subscriber<String>() {

			private long count = 0;
			private Subscription subscription;

			@Override
			public void onSubscribe(Subscription subscription) {
				this.subscription = subscription;
				subscription.request(2);
			}

			@Override
			public void onNext(String t) {
				this.count++;
				if (this.count >= 2) {
					this.count = 0;
					this.subscription.request(2);
				}
			}

			@Override
			public void onError(Throwable t) {
			}

			@Override
			public void onComplete() {
			}
		});
		// Logs the subscription, requests 2 at a time, all elements and
		// completion.
	}
	
	@Test
	public void testSubscription2() {
		// The batching example above can be implemented like this
		// ???????
		flux
		  .log()
		  .map(String::toUpperCase)
		.subscribe(null, 2);
	}
	
	@Test
	public void testParallel() throws Exception {
		flux.log().map(String::toUpperCase).subscribeOn(Schedulers.parallel()).subscribe(2);
		// Logs the subscription, requests 2 at a time, all elements and finally
		// completion.
		Thread.sleep(500L);
	}
	
	@Test
	public void testConcurrent() throws Exception {
		/**
		 To switch the processing of the individual items to separate threads (up to the limit of the pool)
		 	we need to break them out into separate publishers, and for each of those publishers ask for the
		 	result in a background thread. One way to do this is with an operator called flatMap(), which maps
		 	the items to a Publisher (potentially of a different type), and then back to a sequence of the new type
		 */
		Scheduler scheduler = Schedulers.parallel();
		flux.log().flatMap(value -> Mono.just(value.toUpperCase()).subscribeOn(scheduler), 2).subscribe(value -> {
			LOGGER.info("Consumed: " + value);
		});
		// Logs the subscription, requests 2 at a time, all elements and finally
		// completion.
		Thread.sleep(500L);
		/**
		 Note here the use of flatMap() to push the items down into a "child" publisher, where we can control the subscription
		 	per item instead of for the whole sequence. Reactor has built in default behaviour to hang onto a single thread
		 	as long as possible, so we need to be explicit if we want it to process specific items or groups of items in a
		 	background thread. Actually, this is one of a handful of recognized tricks for forcing parallel processing (see the
		 	Reactive Gems issue for more detail - https://github.com/reactor/reactive-streams-commons/issues/21).
		 */
	}
	
	@Test
	public void testPublish() throws Exception {
		/**
		 Flux also has a publishOn() method which is the same, but for the listeners (i.e. onNext() or consumer
		 	callbacks) instead of for the subscriber itself
		 */
		flux.log().map(String::toUpperCase).subscribeOn(Schedulers.newParallel("sub"))
				.publishOn(Schedulers.newParallel("pub"), 2).subscribe(value -> {
					LOGGER.info("Consumed: " + value);
				});
		// Logs the consumed messages in a separate thread.
		Thread.sleep(500L);
		/**
		 Notice that the consumer callbacks (logging "Consumed: ...") are on the publisher thread pub-1-1. If you
		 	take out the subscribeOn() call, you might see all of the 2nd chunk of data processed on the pub-1-1
		 	thread as well. This, again, is Reactor being frugal with threads — if there’s no explicit request to
		 	switch threads it stays on the same one for the next call, whatever that is.
		 	
		 We changed the code in this sample from subscribe(null, 2) to adding a prefetch=2 to the publishOn(). In
		 	this case the fetch size hint in subscribe() would have been ignored.
		 */
	}

}
